$modules = Get-Content input

# Part one
$result1 = 0
foreach($mass in $modules){
    $result1 += [Math]::Floor($mass / 3) - 2
}
Write-Host "Part one: $result1"

# Part two
$result2 = 0
foreach($mass in $modules){
    $fuel = [Math]::Floor($mass / 3) - 2
    $result = $fuel
    # Floor(9 / 3 - 2) == 1
    while($fuel -ge 9) {
        $fuel = [Math]::Floor($fuel / 3) - 2
        $result += $fuel
    }
    $result2 += $result
}
Write-Host "Part two: $result2"
