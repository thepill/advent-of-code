input = open("input.txt").readlines()

rows = []
for i in range(0, 128):
    rows.append(i)
colums = []
for i in range(0, 8):
    colums.append(i)
seat_ids = []


for boarding_pass in input:
    seat = rows
    column = colums
    for r in boarding_pass[:7]:
        half = int(len(seat) / 2)
        if r == "F":
            seat = seat[0:half]
        else:
            seat = seat[half:len(seat)]
    for c in boarding_pass[7:]:
        half = int(len(column) / 2)
        if c == "L":
            column = column[0:half]
        else:
            column = column[half:len(column)]
    seat_id = seat[0] * 8 + column[0]
    seat_ids.append(seat_id)

seat_ids = sorted(seat_ids)

for i in range(1, len(seat_ids)):
    left = seat_ids[i - 1]
    right = seat_ids[i + 1]
    if right - left  > 2:
        print(seat_ids[i] + 1)
        break
