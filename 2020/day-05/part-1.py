input = open("input.txt").readlines()

rows = []
for i in range(0, 128):
    rows.append(i)
colums = []
for i in range(0, 8):
    colums.append(i)
seat_ids = []


for boarding_pass in input:
    seat = rows
    column = colums
    for r in boarding_pass[:7]:
        half = int(len(seat) / 2)
        if r == "F":
            seat = seat[0:half]
        else:
            seat = seat[half:len(seat)]
    for c in boarding_pass[7:]:
        half = int(len(column) / 2)
        if c == "L":
            column = column[0:half]
        else:
            column = column[half:len(column)]
    seat_id = seat[0] * 8 + column[0]
    print(seat, "-", column, " => ",  seat_id)
    seat_ids.append(seat_id)

print(sorted(seat_ids)[len(seat_ids) - 1])
