import re

def is_valid_passport(params):
    if int(params["byr"]) < 1920 or int(params["byr"]) > 2002:
        return False
    
    if int(params["iyr"]) < 2010 or int(params["iyr"]) > 2020:
        return False

    if int(params["eyr"]) < 2020 or int(params["eyr"]) > 2030:
        return False

    hgt = params["hgt"]
    if not re.match(r"^\d+(cm|in)$", hgt):
        return False
    if hgt.endswith("cm"):
        if int(hgt.replace("cm", "")) < 150 or int(hgt.replace("cm", ""))  > 193:
            return False
    elif hgt.endswith("in"):
        if int(hgt.replace("in", ""))  < 59 or int(hgt.replace("in", ""))  > 76:
            return False
    else:
        return False

    if not re.match(r"^#[a-f0-9]{6}$", params["hcl"]):
        return False

    if not re.match(r"^(amb|blu|brn|gry|grn|hzl|oth)$", params["ecl"]):
        return False

    if not re.match("\d{9}", params["pid"]):
        return False

    return True

input = open("input.txt").read()
passports = input.split("\n\n")
valid = 0

for p in passports:
    p = p.replace("\n", " ")

    if "byr" in p and "iyr" in p and "eyr" in p and "hgt" in p and "hcl" in p and "ecl" in p and "pid" in p:
        fields = p.split(" ")
        params = {}

        for field in fields:
            key = field.split(":")[0]
            value = field.split(":")[1]
            params[key] = value

        if is_valid_passport(params):
            valid = valid + 1

print(valid - 1)



"""
byr (Birth Year) - four digits; at least 1920 and at most 2002.
iyr (Issue Year) - four digits; at least 2010 and at most 2020.
eyr (Expiration Year) - four digits; at least 2020 and at most 2030.
hgt (Height) - a number followed by either cm or in:
If cm, the number must be at least 150 and at most 193.
If in, the number must be at least 59 and at most 76.
hcl (Hair Color) - a # followed by exactly six characters 0-9 or a-f.
ecl (Eye Color) - exactly one of: amb blu brn gry grn hzl oth.
pid (Passport ID) - a nine-digit number, including leading zeroes.
cid (Country ID) - ignored, missing or not.
"""