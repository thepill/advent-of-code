input = open("./input.txt").read()

passports = input.split("\n\n")
valid = 0

for p in passports:
    p = p.replace("\n", " ")


    if "byr" in p and "iyr" in p and "eyr" in p and "hgt" in p and "hcl" in p and "ecl" in p and "pid" in p:
        valid = valid + 1
        print(p)
        print("********")

print(valid)

"""
byr
iyr
eyr
hgt 
hcl
ecl
pid
cid
"""