

input = open("./input.txt")
geo = input.readlines()
map = []

for line in geo:
    full = ""
    for r in range(200):
        full = full + line.replace("\n", "")
    map.append(full)

trees = []
pos = 0
goto = [[1,1], [3,1], [5,1], [7,1], [1,2]]

for step in goto:
    right = step[0]
    down = step[1]
    pos = right
    t = 0
    for i in range(down, len(map), down):
        line = map[i]
        if line[pos] == "#":
            t = t + 1
        pos = pos + right
    trees.append(t)


result = 1
for treeCount in trees:
    result = treeCount * result

print(result)