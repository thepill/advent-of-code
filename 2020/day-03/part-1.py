input = open("./input.txt")
geo = input.readlines()
map = []

for line in geo:
    full = ""
    for r in range(200):
        full = full + line.replace("\n", "")
    map.append(full)

trees = 0
pos = 0

for line in map:
    l = line[pos]
    if l == "#":
        trees = trees + 1
    pos = pos + 3

print(trees)