input = open("input.txt").read()

def split(word): 
    return [char for char in word] 

groups = input.split("\n\n")
counts = []
result = 0
for group in groups:
    group = group.replace("\n", "")
    unique = set(split(group))
    counts.append(len(unique))

for c in counts:
    result = result + c

print(result)