input = open("input.txt").read()

def split(word): 
    return [char for char in word] 

groups = input.split("\n\n")
counts = []
result = 0
for group in groups:
    attendes = group.split("\n")
    combined_group = group.replace("\n", "")
    unique = set(split(combined_group))

    for u in unique:
        all_found = True
        for a in attendes:
            if not u in a:
                all_found = False
        if all_found:
            result = result + 1

print(result)