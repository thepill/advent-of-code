import re

input = open("./input.txt")
good = 0
for line in input:
    splitted = line.split(" ")
    policy = splitted[0].replace("-", ",")
    letterIndices = policy.split(",")
    letter = splitted[1].replace(":", "")
    pw = splitted[2]

    pos1Letter = pw[int(letterIndices[0]) - 1]
    pos2Letter = pw[int(letterIndices[1]) - 1]

    if (pos1Letter == letter or pos2Letter == letter) and pos1Letter != pos2Letter:
        good += 1

print(good)