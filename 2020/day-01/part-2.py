from os import truncate


input = open('./input.txt')
numbers = input.readlines()
found = False

for n in numbers:
    for nn in numbers:
        for nnn in numbers:
            sum = int(n) + int(nn) + int(nnn)
            if sum == 2020 and found == False:
                found = True
                multi = int(n) * int(nn) * int(nnn)
                print(n, '*', nn, '*', nnn, ' = ', multi)
