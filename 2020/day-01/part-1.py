from os import truncate


input = open('./input.txt')
numbers = input.readlines()
found = False

for n in numbers:
    for nn in numbers:
        sum = int(n) + int(nn)
        if sum == 2020 and found == False:
            found = True
            multi = int(n) * int(nn)
            print(n, '*', nn, ' = ', multi)
